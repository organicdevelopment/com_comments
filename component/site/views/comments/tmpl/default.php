<? foreach($comments as $comment) : ?>

<?php
if($comment->created_by) {
	$creator = $comment->created_by == $user->id ? @text('You') : $comment->created_by_name ?>&nbsp;<?= @text('wrote');
}else{
	$creator = $comment->guest_name;
}
?>
	
<div class="comment">
    <div class="comment-header">
        <?= $creator ?>
        <time datetime="<?= $comment->created_on ?>" pubdate><?= @helper('date.humanize', array('date' => $comment->created_on)) ?></time>
    </div>
    <p><?= @escape($comment->text) ?></p>
</div>
<? endforeach ?>