<form action="<?= @route('row='.$state->row.'&table='.$state->table) ?>" method="post">
    <input type="hidden" name="row" value="<?= $state->row ?>" />
    <input type="hidden" name="table" value="<?= $state->table ?>" />

	<dl>

		<?php if(!isset($user->id)) : ?>
			<dt><label for="guest_name"><?= @text('User Name') ?></label></dt>
			<dd><input type="text" name="guest_name" value="<?= $state->created_by_name ?>" placeholder="<?= @text('Please add your name here...') ?>" id="created_by_name" /></dd>
		<?php endif; ?>

			<dt><?= @text('Message') ?></dt>
			<dd><textarea type="text" name="text" placeholder="<?= @text('Please add your message here...') ?>" id="new-comment-text"></textarea></dd>
		<br />

		<dd><input class="button" type="submit" value="<?= @text('Submit') ?>"/></dd>

	</dl>
</form>