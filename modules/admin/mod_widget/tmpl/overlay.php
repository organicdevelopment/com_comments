<?
/**
 * @version     $Id: overlay.php 1752 2012-08-11 18:35:24Z johanjanssens $
 * @category	Nooku
 * @package     Nooku_Modules
 * @subpackage  Widget
 * @copyright   Copyright (C) 2011 - 2012 Timble CVBA and Contributors. (http://www.timble.net).
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.nooku.org
 */
?>

<?= @overlay(array('url' => @route($url), 'options' => array('selector' => $module->params->get('selector', 'body')))); ?>